import urllib.request as req
from urllib.parse import quote
from bs4 import BeautifulSoup as bs
import json

GEO_API_KEY = 'f3c2512a34f947bca06485577fc5f836'
MAPS_API_KEY = 'AIzaSyB608w7PC4crlsqooKhKFWRnh8yeORCfbk'

lat_origen = ''
lng_origen = ''
lat_destino = ''
lng_destino = ''
while lat_origen == '' or lng_origen == '':
    origen = input('Origen: ')
    response = req.urlopen('https://api.opencagedata.com/geocode/v1/json?q=' + quote(origen) + '&key=' + GEO_API_KEY)
    data = json.loads(response.read())
    if len(data['results']) != 0:
        origen = data['results'][0]['formatted']
        lat_origen = data['results'][0]['geometry']['lat']
        lng_origen = data['results'][0]['geometry']['lng']
    else:
        print('No encontrado')
while lat_destino == '' or lng_destino == '':
    destino = input('Destino: ')
    response = req.urlopen('https://api.opencagedata.com/geocode/v1/json?q=' + quote(destino) + '&key=' + GEO_API_KEY)
    data = json.loads(response.read())
    if len(data['results']) != 0:
        destino = data['results'][0]['formatted']
        lat_destino = data['results'][0]['geometry']['lat']
        lng_destino = data['results'][0]['geometry']['lng']
    else:
        print('No encontrado')

response = req.urlopen(
    'https://maps.googleapis.com/maps/api/distancematrix/json?&origins=' + str(lat_origen) + ',' + str(
        lng_origen) + '&destinations=' + str(lat_destino) + ',' + str(
        lng_destino) + '&units=metric&key=' + MAPS_API_KEY)
data = json.loads(response.read())
tiempo = data['rows'][0]['elements'][0]['duration']['value']
distancia = data['rows'][0]['elements'][0]['distance']['value']

response = req.urlopen('https://calculartarifataxi.com/madrid')
soup = bs(response.read(), features='html.parser')
info = soup.find('tbody').find_all('tr')[0]
precio_base = info.find_all('td')[1].contents[0].strip()
precio_base = float(precio_base.replace(',', '.')[:-1])
precio_km = info.find_all('td')[2].contents[0].strip()
precio_km = float(precio_km.replace(',', '.')[:-1])
precio_hora = info.find_all('td')[3].contents[0].strip()
precio_hora = float(precio_hora.replace(',', '.')[:-1])

response = req.urlopen('https://cabifyprecios.com/madrid/')
soup = bs(response.read(),features='html.parser')
info = soup.find('tbody').find_all('tr')
cab0_2= float(info[0].find_all('td')[1].contents[0].replace(',', '.')[:-1])
cab2_20=float(info[1].find_all('td')[1].contents[0].replace(',', '.')[:-1])
cab20_80=float(info[2].find_all('td')[1].contents[0].replace(',', '.')[:-1])
cab80=float(info[3].find_all('td')[1].contents[0].replace(',', '.')[:-1])
precio_min = float(soup.find_all('tbody')[2].find('tr').find_all('td')[1].contents[0].replace(',', '.')[:-1])


if tiempo >= 3600:
    precio_total = precio_base+(tiempo/3600)*precio_hora
else:
	precio_total = precio_base + (distancia/1000)*precio_km

if distancia<2000:
	precio_cab = (distancia/1000)*cab0_2
elif distancia>2000 and distancia<20000:
	precio_cab = (distancia/1000)*cab2_20
elif distancia>20000 and distancia<80000:
	precio_cab = (distancia/1000)*cab20_80
else:
	precio_cab = (distancia/1000)*cab80
if precio_cab<precio_min:
	precio_cab = precio_min
	
print('\\n\\t\\t***INFORMACIÓN DE LA RUTA***\\n------------------------------------------------------------')
print('  Origen:', origen)
print('  Destino:', destino)
print('  Distancia:', round(distancia/1000,3),'km')
print('  Tiempo estimado:', round(tiempo/60,0),'min')
print('  Precio estimado del taxi:',round(precio_total,2),'€')
print('  Precio estimado del cabify:',round(precio_cab,2),'€')
print('------------------------------------------------------------')
